# Scripts

This folder contains scripts I use across various systems that make my life easier.

Note: These are written in posix shell scripting (or as close to as I can manage) 
it's important to me that these be as portable as possible even if I am using 
utilities that aren't readily available on all platforms e.g. nvim. 
It should be fairly easy to install just those utilities and get going.
